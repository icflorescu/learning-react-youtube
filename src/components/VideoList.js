import React from 'react';

import VideoListItem from './VideoListItem';

export default props => (
  <div class="mui-row">
    {props.items.map(item =>
      <VideoListItem
        key={item.id}
        videoId={item.id} thumbnailUrl={item.thumbnailUrl}
        handleSelect={props.handleSelect} />
    )}
  </div>
);
