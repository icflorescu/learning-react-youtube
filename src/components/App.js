import React, { Component } from 'react';

import axios from 'axios';

import Appbar    from './Appbar';
import Video     from './Video';
import VideoList from './VideoList';

const API_KEY = 'AIzaSyAb2536hfdQC99laC4stE9alKSudxqoggk';

export default class App extends Component {
  state = {
    items: [],
    selectedItem: {}
  };

  handleSearch = (terms) => {
    axios.get('https://www.googleapis.com/youtube/v3/search', {
      params: {
        part: 'snippet', key: API_KEY, type: 'video', q: terms, maxResults: 12
      }
    }).then((res) => {
      const items = res.data.items.map(item => {
        return {
          id:           item.id.videoId,
          title:        item.snippet.title,
          publishedAt:  item.snippet.publishedAt,
          thumbnailUrl: item.snippet.thumbnails.high.url
        };
      });
      this.setState({
        items,
        selectedVideoId: items[0].id,
        autoPlay: false
      });
    });
  };

  handleSelect = (videoId) => {
    this.setState({
      items: this.state.items,
      selectedVideoId: videoId,
      autoPlay: true
    });
  };

  render() {
    return (
      <div>
        <Appbar handleSearch={this.handleSearch} />
        <div class="mui-container">
          <Video videoId={this.state.selectedVideoId} autoPlay={this.state.autoPlay} />
          <VideoList items={this.state.items} handleSelect={this.handleSelect} />
        </div>
      </div>
    );
  }
};
