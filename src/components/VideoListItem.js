import React, { Component } from 'react';

export default class VideoListItem extends Component {
  handleClick = e => {
    this.props.handleSelect(this.props.videoId);
  };

  render() {
    return (
      <div class="mui-col-xs-12 mui-col-md-4 mui-col-lg-3">
        <img class="video-list-item-img" src={this.props.thumbnailUrl} onClick={this.handleClick} />
      </div>
    );
  }
};
