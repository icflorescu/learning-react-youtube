import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Video extends Component {
  componentDidUpdate() {
    if (this.props.autoPlay) {
      ReactDOM.findDOMNode(this).scrollIntoView();
    }
  }

  render() {
    if (!this.props.videoId) return (
      <div class="video-empty">Discover awesome videos on YouTube.</div>
    );

    let src = `https://www.youtube.com/embed/${this.props.videoId}`;
    if (this.props.autoPlay) src += '?autoplay=1';

    return (
      <div class="mui-row">
        <div class="mui-col-xs-12">
          <div class="video-container">
            <iframe class="video-player" src={src} allowFullScreen />
          </div>
        </div>
      </div>
    );
  }
};
