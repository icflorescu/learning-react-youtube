import React, { Component } from 'react';
import debounce from 'lodash.debounce';

export default class Title extends Component {
  state = { terms: '' };

  handleSearchDebounced = debounce(this.props.handleSearch, 500);

  handleChange = e => {
    const terms = e.target.value;
    this.setState({ terms })
    this.handleSearchDebounced(terms);
    e.preventDefault();
  };

  render() {
    return (
      <div class="mui-appbar">
        <form class="mui-container mui--appbar-height appbar-container" onSubmit={this.handleChange}>
          <input
            type="search" class="appbar-input" autoFocus placeholder="Search for videos..."
            value={this.state.terms} onChange={this.handleChange} />
          <i class="material-icons appbar-icon-search">search</i>
        </form>
      </div>
    );
  }
};
